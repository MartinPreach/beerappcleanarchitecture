package app.social.beersapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.social.beersapp.data.model.BeersModel
import app.social.beersapp.domain.GetBeersUseCase
import kotlinx.coroutines.launch

class BeersViewModel:ViewModel() {

    val beersModel = MutableLiveData<BeersModel>()

    val isLoading = MutableLiveData<Boolean>()

    var getBeersUseCase = GetBeersUseCase()

    fun onCreate() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getBeersUseCase()
            if(!result.isNullOrEmpty()){
                beersModel.postValue(result[0])
                isLoading.postValue(false)
            }
        }
    }

}