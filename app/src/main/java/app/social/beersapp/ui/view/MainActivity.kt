package app.social.beersapp.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import app.social.beersapp.databinding.ActivityMainBinding
import app.social.beersapp.ui.viewmodel.BeersViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding

    private val beersViewModel: BeersViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        beersViewModel.onCreate()

        beersViewModel.beersModel.observe(this, Observer {
            binding.tvprueba.text = it.name
        })

        beersViewModel.isLoading.observe(this, Observer {
            binding.loading.isVisible = it
        })

    }
}
