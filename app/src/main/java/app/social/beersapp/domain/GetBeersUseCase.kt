package app.social.beersapp.domain

import app.social.beersapp.data.BeersRepository
import app.social.beersapp.data.model.BeersModel

class GetBeersUseCase {

    private val repository = BeersRepository()
    suspend operator fun invoke():List<BeersModel>? = repository.getAllBeers()

}