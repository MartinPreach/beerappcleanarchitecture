package app.social.beersapp.data.network

import app.social.beersapp.data.model.BeersModel
import retrofit2.Response
import retrofit2.http.GET

interface BeersApiClient {
    @GET("/.beers")
    suspend fun getAllBeers():Response<List<BeersModel>>
}