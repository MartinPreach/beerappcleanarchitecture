package app.social.beersapp.data.model

data class BeersModel (val name:String, val tagline:String, val first_brewed:String)