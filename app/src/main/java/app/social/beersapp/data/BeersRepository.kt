package app.social.beersapp.data

import app.social.beersapp.data.model.BeersModel
import app.social.beersapp.data.model.BeersProvider
import app.social.beersapp.data.network.BeersService

class BeersRepository {

    private val api = BeersService()

    suspend fun getAllBeers():List<BeersModel>{
        val response = api.getBeers()
        BeersProvider.beers = response
        return response
    }
}