package app.social.beersapp.data.network

import app.social.beersapp.core.RetrofitHelper
import app.social.beersapp.data.model.BeersModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BeersService {
    private val retrofit = RetrofitHelper.getRetrofit()
    suspend fun getBeers(): List<BeersModel> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(BeersApiClient::class.java).getAllBeers()
            response.body() ?: emptyList()
        }
    }
}